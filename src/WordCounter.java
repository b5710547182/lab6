import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class has its map that will be able to remember that it is called.
 * @author Thanawit Gerdprasert.
 *
 */
public class WordCounter {

	Map<String,Integer> numbers;
	/**
	 * Initialize the Map for the further use.
	 */
	public WordCounter()
	{
		numbers = new HashMap<String , Integer>( );
	}
	/**
	 * Get the current Map from this class.
	 * @return the Map in this class.
	 */
	public Map<String,Integer> getMap()
	{
		return this.numbers;
	}
	/**
	 * Add the word to the map and update the count of that word that it used to called.
	 * @param word that will be called to get the count.
	 */
	public void addWords(String word)
	{
		String wordToAdd = word.toLowerCase();
		
		if(numbers.containsKey(wordToAdd))
		{
			int temp = numbers.get(wordToAdd);
			temp++;
			numbers.remove(wordToAdd);
			numbers.put(wordToAdd,temp);
		}
		else
		{
			int temp =1;
			numbers.put(wordToAdd,temp );
		}
		
	}
	/**
	 * Return the Set of the current Map.
	 * @return the Set of the current Map.
	 */
	public Set<String> getWords()
	{
		return numbers.keySet();
	}
	/**
	 * Return the count of the current word that called.
	 * @param word that want its count to be checked.
	 * @return the count of that word,
	 */
	public int getCount(String word)
	{
			return numbers.get(word.toLowerCase());
	}
	/**
	 * Convert Map to Array of String and return after sorted.
	 * @return the Array of String after sorted.
	 */
	public String[] getSortedWords()
	{
		Set<String> tempSet = numbers.keySet();
		ArrayList<String> newList = new ArrayList<String>(tempSet);
		Collections.sort(newList);
		String [] returnArray = new String[newList.size()];
		for(int i =0 ; i<returnArray.length;i++)
		{
			returnArray[i] = newList.get(i);
		}
		return returnArray;
		
	}
}
