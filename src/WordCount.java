
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
public class WordCount {

	final static String DELIMS = "[\\s,.\\?!\"():;]+";
	public static void main(String[] args) throws IOException {
		String FILE_URL = "https://gist.githubusercontent.com/anonymous/0f0374e431bb88f403c9/raw/b8ad310a887093bca6bcc54192677c24bb00a7a2/AliceInWonderland.txt";
		URL url = new URL(FILE_URL);
		InputStream input = url.openStream();
		Scanner scanner = new Scanner( input );
		scanner.useDelimiter(DELIMS);
		WordCounter counter = new WordCounter();
		ArrayList<Integer> wordCounter = new ArrayList<Integer>();
		//create the counter of both word and value.
		while(scanner.hasNext())
		{
			String temp = scanner.next();
			counter.addWords(temp);
		}

		String[] printingWord = counter.getSortedWords();
		//print first 20 word
		for(int i= 0 ; i < 20;i++)
			System.out.println(printingWord[i] +" "+ counter.getCount(printingWord[i]));

		for(int i =0 ; i<printingWord.length;i++)
		{
			wordCounter.add(counter.getCount(printingWord[i]));
		}

		Collections.sort(wordCounter);
		Collections.reverse(wordCounter);
		System.out.println("______________________________");
		int tempPrintRunner =0;
		
		for(int i =0 ; i<printingWord.length;i++)
		{
			if(counter.getMap().get(printingWord[i])==wordCounter.get(0))
			{
				System.out.println(printingWord[i] +" " + wordCounter.get(0));
				counter.getMap().remove(printingWord[i]);
				wordCounter.remove(0);
				tempPrintRunner++;
				i=-1;
				if(tempPrintRunner==20)
					break;
			}	
		}


	}

}
